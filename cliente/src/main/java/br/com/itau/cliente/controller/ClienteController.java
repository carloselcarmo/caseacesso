package br.com.itau.cliente.controller;

import br.com.itau.cliente.model.Cliente;
import br.com.itau.cliente.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController
{
    @Autowired
    ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente criar(@RequestBody @Valid Cliente cliente)
    {
        return  clienteService.criar(cliente);
    }

    @GetMapping("/{id}")
    public Cliente buscarPorId(@PathVariable(name = "id") Integer id)
    {
        return clienteService.buscarPorId(id);
    }
}
