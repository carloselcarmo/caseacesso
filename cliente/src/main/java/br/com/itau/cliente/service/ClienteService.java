package br.com.itau.cliente.service;

import br.com.itau.cliente.exception.ClienteNaoEncontradoException;
import br.com.itau.cliente.model.Cliente;
import br.com.itau.cliente.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService
{
    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente criar (Cliente porta)
    {
        System.out.println(System.currentTimeMillis() + " - Tentativa de salvar o cliente " + porta.getNome());
        return  clienteRepository.save(porta);
    }

    public Cliente buscarPorId(Integer id)
    {
        System.out.println(System.currentTimeMillis() + " - Buscaram o cliente " + id);
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);

        if(clienteOptional.isPresent())
        {
            Cliente cliente = clienteOptional.get();
            return  cliente;
        }
        else
        {
            throw new ClienteNaoEncontradoException();
        }
    }
}
