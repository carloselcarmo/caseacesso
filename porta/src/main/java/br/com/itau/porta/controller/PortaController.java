package br.com.itau.porta.controller;

import br.com.itau.porta.model.Porta;
import br.com.itau.porta.service.PortaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/porta")
public class PortaController
{
    @Autowired
    PortaService portaService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Porta criar(@RequestBody @Valid Porta porta)
    {
        return  portaService.criar(porta);
    }

    @GetMapping("/{id}")
    public Porta buscarPorId(@PathVariable(name = "id") int id)
    {
        return portaService.buscarPorId(id);
    }
}
