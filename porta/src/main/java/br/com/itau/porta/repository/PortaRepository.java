package br.com.itau.porta.repository;

import br.com.itau.porta.model.Porta;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PortaRepository extends JpaRepository<Porta,Integer > {
}
