package br.com.itau.porta.service;

import br.com.itau.porta.exception.PortaNaoEncontradaException;
import br.com.itau.porta.model.Porta;
import br.com.itau.porta.repository.PortaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PortaService
{
    @Autowired
    private PortaRepository portaRepository;

    public Porta criar (Porta porta)
    {
        System.out.println(System.currentTimeMillis() + " - Tentativa de salvar a porta " + porta.getSala());
        return  portaRepository.save(porta);
    }

    public Porta buscarPorId(Integer id)
    {
        System.out.println(System.currentTimeMillis() + " - Buscaram a porta " + id);
        Optional<Porta> portaOptional = portaRepository.findById(id);

        if(portaOptional.isPresent())
        {
            Porta porta = portaOptional.get();
            return  porta;
        }
        else
        {
            throw new PortaNaoEncontradaException();
        }
    }
}
