package br.com.itau.acesso.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "CLIENTE", configuration = ClienteClientConfiguration.class)
public interface ClienteClient
{
    @GetMapping("/cliente/{idCliente}")
    Cliente buscarPorId(@PathVariable Integer idCliente);
}