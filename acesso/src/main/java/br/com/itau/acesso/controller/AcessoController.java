package br.com.itau.acesso.controller;

import br.com.itau.acesso.DTO.AcessoEntrada;
import br.com.itau.acesso.DTO.AcessoSaida;
import br.com.itau.acesso.service.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/acesso")
public class AcessoController
{
    @Autowired
    AcessoService acessoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public AcessoSaida criar(@RequestBody @Valid AcessoEntrada acesso)
    {
        System.out.println(System.currentTimeMillis() + " - Tentaram criar o acesso a porta "+ acesso.getPorta_id() + " para o cliente " + acesso.getCliente_id());
        return  acessoService.criar(acesso);
    }

    @GetMapping("/{cliente_id}/{porta_id}")
    public AcessoSaida buscarPorId(@PathVariable(name = "cliente_id") Integer clienteId, @PathVariable(name = "porta_id") Integer portaId)
    {
        System.out.println(System.currentTimeMillis() + " - Buscaram o acesso a porta "+ portaId + " do cliente " + clienteId);
        return acessoService.buscarPorClienteIdPortaId(clienteId, portaId);
    }

    @DeleteMapping("/{cliente_id}/{porta_id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void excluir(@PathVariable(name = "cliente_id") Integer clienteId, @PathVariable(name = "porta_id") Integer portaId)
    {
        System.out.println(System.currentTimeMillis() + " - Excluíram o acesso a porta "+ portaId + " do cliente " + clienteId);
        acessoService.excluir(clienteId, portaId);
    }
}
