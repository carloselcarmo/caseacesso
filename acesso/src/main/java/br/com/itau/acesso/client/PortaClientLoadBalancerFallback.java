package br.com.itau.acesso.client;

import br.com.itau.acesso.exception.APIPortaNaoDisponivelException;
import com.netflix.client.ClientException;

public class PortaClientLoadBalancerFallback implements PortaClient
{
    private Exception ex;

    public PortaClientLoadBalancerFallback(Exception ex)
    {
        this.ex = ex;
    }

    @Override
    public Porta buscarPorId(Integer idPorta)
    {
        if(ex.getCause() instanceof ClientException)
        {
            throw new APIPortaNaoDisponivelException();
        }
        throw (RuntimeException) ex;
    }
}
