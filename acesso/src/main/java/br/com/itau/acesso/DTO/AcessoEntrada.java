package br.com.itau.acesso.DTO;

import javax.validation.constraints.NotNull;

public class AcessoEntrada
{
    @NotNull(message =  "Cliente não pode ser nulo")
    private Integer cliente_id;

    @NotNull(message =  "Porta não pode ser nula")
    private Integer porta_id;

    public Integer getCliente_id() {
        return cliente_id;
    }

    public void setCliente_id(Integer cliente_id) {
        this.cliente_id = cliente_id;
    }

    public Integer getPorta_id() {
        return porta_id;
    }

    public void setPorta_id(Integer porta_id) {
        this.porta_id = porta_id;
    }

    public AcessoEntrada()
    {
    }
}
