package br.com.itau.acesso.repository;

import br.com.itau.acesso.model.Acesso;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AcessoRepository extends JpaRepository<Acesso,Integer >
{
    Acesso findByClienteIdAndPortaId(Integer clienteId, Integer portaId);
}
