package br.com.itau.acesso.service;

import br.com.itau.acesso.DTO.AcessoEntrada;
import br.com.itau.acesso.DTO.AcessoSaida;
import br.com.itau.acesso.DTO.InformacaoAcesso;
import br.com.itau.acesso.client.Cliente;
import br.com.itau.acesso.client.ClienteClient;
import br.com.itau.acesso.client.Porta;
import br.com.itau.acesso.client.PortaClient;
import br.com.itau.acesso.exception.AcessoNaoEncontradoException;
import br.com.itau.acesso.exception.ClienteJaPossuiAcessoException;
import br.com.itau.acesso.model.Acesso;
import br.com.itau.acesso.producer.AcessoProducer;
import br.com.itau.acesso.repository.AcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AcessoService
{
    @Autowired
    private AcessoRepository acessoRepository;

    @Autowired
    private ClienteClient clienteClient;

    @Autowired
    private PortaClient portaClient;

    @Autowired
    private AcessoProducer acessoProducer;

    public AcessoSaida criar(AcessoEntrada acesso)
    {
        Porta porta = portaClient.buscarPorId(acesso.getPorta_id());

        Cliente cliente = clienteClient.buscarPorId(acesso.getCliente_id());

        Acesso acessoDb = acessoRepository.findByClienteIdAndPortaId(acesso.getCliente_id(), acesso.getPorta_id());

        if(acessoDb != null)
        {
            throw new ClienteJaPossuiAcessoException();
        }
        else
        {
            Acesso acessoInclusao = new Acesso();
            acessoInclusao.setClienteId(acesso.getCliente_id());
            acessoInclusao.setPortaId(acesso.getPorta_id());

            acessoInclusao = acessoRepository.save(acessoInclusao);

            return new AcessoSaida(acessoInclusao);
        }
    }

    public AcessoSaida buscarPorClienteIdPortaId(Integer clienteid, Integer portaId)
    {
        Acesso acesso = acessoRepository.findByClienteIdAndPortaId(clienteid, portaId);

        if(acesso != null)
        {
            acessoProducer.enviarAoKafka(new InformacaoAcesso(clienteid, portaId, true));
            return new AcessoSaida(acesso);
        }
        else
        {
            acessoProducer.enviarAoKafka(new InformacaoAcesso(clienteid, portaId, false));
            throw new AcessoNaoEncontradoException();
        }
    }

    private Acesso buscarPorId(Integer clienteid, Integer portaId)
    {
        Acesso acesso = acessoRepository.findByClienteIdAndPortaId(clienteid, portaId);

        if(acesso != null)
        {
            return acesso;
        }
        else
        {
            throw new AcessoNaoEncontradoException();
        }
    }

    public void excluir(Integer clienteid, Integer portaId)
    {
        Acesso acesso = buscarPorId(clienteid, portaId);
        acessoRepository.delete(acesso);
    }
}
