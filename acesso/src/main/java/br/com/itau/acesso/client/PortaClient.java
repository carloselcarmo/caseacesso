package br.com.itau.acesso.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "PORTA", configuration = PortaClientConfiguration.class)
public interface PortaClient
{
    @GetMapping("/porta/{idPorta}")
    Porta buscarPorId(@PathVariable Integer idPorta);
}
