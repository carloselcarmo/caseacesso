package br.com.itau.acesso.client;

import feign.Feign;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class PortaClientConfiguration
{
    @Bean
    public ErrorDecoder getPortaClientDecoder()
    {
        return new PortaCodeDecoder();
    }

    @Bean
    public Feign.Builder builder()
    {
        FeignDecorators decorators =  FeignDecorators.builder()
                .withFallback(new PortaClientFallback(), feign.RetryableException.class)
                .withFallbackFactory(PortaClientLoadBalancerFallback::new, RuntimeException.class)
                .build();

        return Resilience4jFeign.builder(decorators);
    }
}
