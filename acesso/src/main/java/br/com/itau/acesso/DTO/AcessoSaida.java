package br.com.itau.acesso.DTO;

import br.com.itau.acesso.model.Acesso;

public class AcessoSaida
{
    private Integer cliente_id;

    private Integer porta_id;

    public Integer getCliente_id() {
        return cliente_id;
    }

    public void setCliente_id(Integer cliente_id) {
        this.cliente_id = cliente_id;
    }

    public Integer getPorta_id() {
        return porta_id;
    }

    public void setPorta_id(Integer porta_id) {
        this.porta_id = porta_id;
    }

    public AcessoSaida() {
    }

    public AcessoSaida(Acesso acesso)
    {
        setCliente_id(acesso.getClienteId());
        setPorta_id(acesso.getPortaId());
    }
}
