package br.com.itau.acesso.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code= HttpStatus.BAD_GATEWAY, reason = "A API de Portas não está disponível")
public class APIPortaNaoDisponivelException extends RuntimeException  {
}
