package br.com.itau.acesso.client;

import br.com.itau.acesso.exception.APIPortaNaoDisponivelException;

public class PortaClientFallback implements PortaClient {
    @Override
    public Porta buscarPorId(Integer idPorta) {
        throw new APIPortaNaoDisponivelException();
    }
}