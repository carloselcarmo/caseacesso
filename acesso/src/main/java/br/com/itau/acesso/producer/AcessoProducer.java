package br.com.itau.acesso.producer;

import br.com.itau.acesso.DTO.InformacaoAcesso;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class AcessoProducer
{
    @Autowired
    private KafkaTemplate<String, InformacaoAcesso> producer;

    public void enviarAoKafka(InformacaoAcesso acesso)
    {
        producer.send("spec4-carlos-eduardo-1", acesso);
    }
}
