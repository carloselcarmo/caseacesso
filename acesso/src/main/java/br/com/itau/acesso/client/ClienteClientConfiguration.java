package br.com.itau.acesso.client;

import feign.Feign;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class ClienteClientConfiguration
{
    @Bean
    public ErrorDecoder getClienteClientDecoder()
    {
        return new ClienteCodeDecoder();
    }

    @Bean
    public Feign.Builder builder()
    {
        FeignDecorators decorators =  FeignDecorators.builder()
                .withFallback(new ClienteClientFallback(), feign.RetryableException.class)
                .withFallbackFactory(ClienteClientLoadBalancerFallback::new, RuntimeException.class)
                .build();

        return Resilience4jFeign.builder(decorators);
    }
}
