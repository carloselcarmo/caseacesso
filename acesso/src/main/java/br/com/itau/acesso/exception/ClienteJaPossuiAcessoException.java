package br.com.itau.acesso.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code= HttpStatus.CONFLICT, reason = "Cliente já possui acesso a essa porta")
public class ClienteJaPossuiAcessoException extends RuntimeException {
}