package br.com.itau.acesso.client;

import br.com.itau.acesso.exception.PortaNaoEncontradaException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class PortaCodeDecoder implements ErrorDecoder {
    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response)
    {
        if(response.status() == 404)
        {
            return new PortaNaoEncontradaException();
        }
        return errorDecoder.decode(s, response);
    }
}
