package br.com.itau.acesso.client;

import br.com.itau.acesso.exception.APIClienteNaoDisponivelException;

public class ClienteClientFallback implements ClienteClient {
    @Override
    public Cliente buscarPorId(Integer idCliente) {
        throw new APIClienteNaoDisponivelException();
    }
}