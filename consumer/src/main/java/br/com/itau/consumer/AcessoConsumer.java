package br.com.itau.consumer;

import br.com.itau.acesso.DTO.InformacaoAcesso;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.serializer.DelegatingSerializer;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Component
public class AcessoConsumer
{
    @KafkaListener(topics = "spec4-carlos-eduardo-1", groupId = "teste")
    public void receber(@Payload InformacaoAcesso acesso) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException
    {
        salvarCSV(acesso);
    }

    private List<InformacaoAcesso> lerCSV() throws IOException
    {
        Reader reader = Files.newBufferedReader(Paths.get("acesso.csv"));

        CsvToBean<InformacaoAcesso> csvToBean = new CsvToBeanBuilder(reader)
                .withType(InformacaoAcesso.class)
                .build();

        return csvToBean.parse();
    }

    private void salvarCSV(InformacaoAcesso acesso) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException
    {
        File file = new File("acesso.csv");

        List<InformacaoAcesso> acessos;
        if(file.exists())
        {
            acessos = lerCSV();
        }
        else
        {
            acessos = new ArrayList<>();
        }

        acessos.add(acesso);

        Writer writer = Files.newBufferedWriter(Paths.get("acesso.csv"));
        StatefulBeanToCsv<InformacaoAcesso> beanToCsv = new StatefulBeanToCsvBuilder(writer).build();

        beanToCsv.write(acessos);

        writer.flush();
        writer.close();
    }
}