package br.com.itau.acesso.DTO;

public class InformacaoAcesso
{
    private Integer clienteId;

    private Integer portaId;

    private Boolean possuiAcesso;

    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }

    public Integer getPortaId() {
        return portaId;
    }

    public void setPortaId(Integer portaId) {
        this.portaId = portaId;
    }

    public Boolean getPossuiAcesso() {
        return possuiAcesso;
    }

    public void setPossuiAcesso(Boolean possuiAcesso) {
        this.possuiAcesso = possuiAcesso;
    }

    public InformacaoAcesso() {
    }

    public InformacaoAcesso(Integer clienteId, Integer portaId, Boolean possuiAcesso) {
        this.clienteId = clienteId;
        this.portaId = portaId;
        this.possuiAcesso = possuiAcesso;
    }
}
